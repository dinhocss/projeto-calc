let espaco = document.querySelector(".caixa-texto");
let media = document.querySelector(".caixa-media");

let soma = 0;
let cont = 0;

function adicionar_nota(){
    let input = document.querySelector(".nota");
    let numeros = input.value;
    numeros = parseFloat(numeros);
    soma = parseFloat(soma);
    if(numeros>=0 && numeros <= 9){
        let texto = document.createElement("p");
        let div = document.createElement("div");
        texto.innerText = "Nota: " + numeros;
        div.append(texto);
        espaco.append(div);
        cont++;
        soma=soma+numeros;
        console.log(soma);
        div.style.fontSize = "20px";
        div.style.fontFamily = "Serif";
        
    }
    else{
        alert("Nota inválida. Insira a nota novamente.")
    }
}

let adicionar = document.querySelector(".btm-adicionar");
adicionar.addEventListener("click",()=> adicionar_nota())

function adicionar_media(){
    let div = document.createElement("div");
    let med = soma/cont;
    med.innerText = "Media: " + med;
    div.append(med);
    media.append(div);
    div.style.fontSize = "20px";
    div.style.fontFamily = "Serif";
}

let calcmedia = document.querySelector(".btm-media");
calcmedia.addEventListener("click",()=>adicionar_media());